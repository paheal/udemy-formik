import React from "react";
import "./App.css";
// import SimpleForm from "./forms/SimpleForm";
import SignupFrom from "./forms/SignupForm";

function App() {
  return (
    <div className="App">
      <div className="container">
        {/* <SimpleForm /> */}
        <SignupFrom />
      </div>
    </div>
  );
}

export default App;
