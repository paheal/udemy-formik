import React from "react";
import Select from "react-select";

const DropList = props => {
  function handleChange(value) {
    console.log(value);
    props.onChange("topics", value);
  }

  function handleBlur() {
    props.onBlur("topics", true);
  }
  return (
    <Select
      value={props.value}
      onChange={handleChange}
      onBlur={handleBlur}
      options={props.options}
      isMulti={true}
    />
  );
};

export default DropList;
