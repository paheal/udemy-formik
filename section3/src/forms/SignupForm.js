import React from "react";
import { withFormik, ErrorMessage, Field, Form } from "formik";
import DropList from "./DropList";
import * as Yup from "yup";
import Error from "./Error";

const formikWrapper = withFormik({
  mapPropsToValues: () => ({
    username: "",
    email: "",
    topics: []
  }),
  handleSubmit: (values, { setSubmitting }) => {
    const payload = {
      ...values,
      topics: values.topics.map(t => t.value)
    };
    setTimeout(() => {
      alert(JSON.stringify(payload, null, 2));
      setSubmitting(false);
    }, 3000);
  },
  validationSchema: Yup.object().shape({
    username: Yup.string().required("Please enter username"),
    email: Yup.string()
      .email("Please enter a valid email")
      .required("Please enter you email address."),
    topics: Yup.array()
      .min(2, "You must select at least 2 topics.")
      .of(
        Yup.object().shape({
          value: Yup.string().required(),
          label: Yup.string().required()
        })
      )
  })
});

const options = [
  { value: "Art", label: "Art" },
  { value: "Sports", label: "Sports" },
  { value: "Technology", label: "Technology" },
  { value: "Science", label: "Science" },
  { value: "Machine Learning", label: "Machine Learning" }
];

const SignupForm = props => {
  const {
    values,
    handleChange,
    handleBlur,
    setFieldValue,
    setFieldTouched,
    handleSubmit,
    handleReset,
    isSubmitting,
    dirty
  } = props;

  return (
    <Form className="p-5">
      <h1>Sign Up Form</h1>
      <div className="form-group">
        <label className="left">User name:</label>
        <Field
          name="username"
          type="text"
          placeholder="Enter username"
          className="form-control"
        />
        <ErrorMessage component={Error} name="username" />
      </div>

      <div className="form-group">
        <label>Email:</label>
        <Field
          name="email"
          type="email"
          placeholder="Enter email address"
          className="form-control"
        />
        <ErrorMessage component={Error} name="email" />
      </div>

      <div className="form-group">
        <label>Favorite Topics</label>
        <DropList
          options={options}
          value={values.topics}
          onChange={setFieldValue}
          onBlur={setFieldTouched}
        />

        <ErrorMessage component={Error} name="topics" />
      </div>

      <span className="pr-1">
        <button
          className="btn btn-secondary"
          onClick={handleReset}
          disabled={!dirty || isSubmitting}
        >
          Reset
        </button>
      </span>

      <span>
        <button
          type="submit"
          className="btn btn-primary"
          disabled={isSubmitting}
        >
          Submit
        </button>
      </span>
    </Form>
  );
};

const EnhancedForm = formikWrapper(SignupForm);
export default EnhancedForm;
