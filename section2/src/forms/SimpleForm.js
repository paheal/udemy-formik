import React from "react";
import { Formik, ErrorMessage } from "formik";

const SimpleForm = () => {
  return (
    <Formik
      initialValues={{ name: "" }}
      onSubmit={(formValues, { setSubmitting }) => {
        setTimeout(() => {
          console.log("FORM VALUES : ", formValues);
          setSubmitting(false);
        }, 5000);
      }}
      validate={values => {
        let errors = {};

        if (!values.name) {
          errors.name = "Please enter a name";
        }
        if (
          values.name &&
          (values.name.length < 3 || values.name.length > 100)
        ) {
          errors.name = "Name value must be between 3 and 99 characters long.";
        }

        return errors;
      }}
      render={({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        touched,
        values
      }) => (
        <form onSubmit={handleSubmit}>
          <input
            onBlur={handleBlur}
            onChange={handleChange}
            value={values.name}
            type="text"
            name="name"
            placeholder="Enter your name"
          />
          <button disabled={isSubmitting} type="submit">
            Submit
          </button>
          <ErrorMessage name="name" />
        </form>
      )}
    />
  );
};

export default SimpleForm;
