import React from "react";
import "./App.css";
import SimpleForm from "./forms/SimpleForm";

function App() {
  return (
    <div className="App">
      <div className="container">
        <SimpleForm />
      </div>
    </div>
  );
}

export default App;
